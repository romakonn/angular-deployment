# AngularDeployment

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. 
Use the `--prod` flag for a production build and the `--configuration=staging` flag - for a staging build. 

## Running unit tests (karma is also set for chrome headless testing option)

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io) and run `ng test-ci` to execute them with chrome headless option

## Running end-to-end tests (end to end is set for chrome headless testing option)

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/). 
In case of set option tests will be executed with chrome headless option. If chrome defaul option has to be executed, 
please remove following option from protractor.conf.js:  

~~~
chromeOptions: {
    args: [ "--headless" ]
},
~~~

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
